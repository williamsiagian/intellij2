import java.util.regex.Pattern

fun main (){

    val a = intArrayOf(1,4)
    val b = intArrayOf(3,4)
    val c = intArrayOf(3,10)

//    solution(arrayOf(a,b,c))
//    println(solution(arrayOf(a,b,c)).toString())
//    println(solution(987))

    println(  solution(8,4,7))


}

fun solution(n: Int, a: Int, b: Int): Int {
    var answer = 0

    var c = mutableListOf<Int>()
    var d = mutableListOf<Int>()

    var f = false
    var round = 0

    for(i in 1..n){
        c+=i
    }

    while (!f){
        round+=1

        for(i in 0 until c.size-1  step 2){

            if(i<= c.size-2){
                if((c[i]==a && c[i+1]==b)||(c[i]==b && c[i+1]==a)){
                    f = true

                }else if(c[i]==a||c[i]==b){
                    d+=c[i]

                }else if(c[i+1]==a||c[i+1]==b){
                    d+=c[i+1]

                }else{
                    d+=c[i]
                }
            }
        }
        c=d.toMutableList()
        d.clear()
    }

    return round
}

//fun solution(n: Int): Int {
//    val a = n.toString().toList()
//    var total = 0
//
//    for(i in a){
//        total+=i.toInt()
//    }
//
//    return total
//}

//fun solution(v: Array<IntArray>): IntArray {
//
//    var a = 0
//    var b = 0
//
//    a = if (v[0][0] == v[1][0]) {
//        v[2][0]
//    } else {
//        if(v[0][0]==v[2][0]){
//            v[1][0]
//        }else{
//            v[0][0]
//        }
//    }
//
//    b = if (v[0][1] == v[1][1]) {
//        v[2][1]
//    } else {
//        if(v[0][1] == v[2][1]){
//            v[1][1]
//        }else{
//            v[0][1]
//        }
//    }
//    println(a)
//    println(b)
//
//    return intArrayOf(a, b)
//}

fun searchingChallenge(input : String) : Int{

    var totalNumbers : Int = 0
    var totalLetters : Int = 0
    var resultInt : Int = 0
    var result : Double = 0.000

    for(i in input){

        if(i.isDigit()){
            totalNumbers += i.digitToInt()

        }else if(i.isLetter()){
            totalLetters += 1
        }
    }

    result = totalNumbers/totalLetters.toDouble()
    resultInt = result.toInt()
    return if(result-resultInt < 0.5){
        resultInt
    }else{
        resultInt+1
    }

}

fun isValidPasswordFormat(password: String): Boolean {

    return if(password.lowercase().contains("password")){
        false
    }else{
        val passwordREGEX = Pattern.compile("^" +
//                "(?=.*[0-9])" +         //at least 1 digit
                "(?=.*[A-Z])" +         //at least 1 upper case letter
//                "(?=.*[a-zA-Z])" +      //any letter
//                "(?=.*[@#$%^&+=])" +    //at least 1 special character
//                ".{8,31}" +               //at least 8 characters
                "$");
        passwordREGEX.matcher(password).matches()
    }
}

//fun isValidPasswordFormat(password: String): Boolean {
//
//    return if(password.lowercase().contains("password")){
//        false
//    }else{
//        val passwordREGEX = Pattern.compile("^" +
//                "(?=.*[0-9])" +         //at least 1 digit
//                "(?=.*[A-Z])" +         //at least 1 upper case letter
//                "(?=.*[a-zA-Z])" +      //any letter
//                "(?=.*[@#$%^&+=])" +    //at least 1 special character
//                ".{8,31}" +               //at least 8 characters
//                "$");
//        passwordREGEX.matcher(password).matches()
//    }
//}

fun printBinarySquare( input : Int){

    for( i  in 1..input){

        if(i%2==0){
            for( j in 1..input){
                if (j%2==0){
                    print("0")
                }else{
                    print("1")
                }
            }
        }else{
            for(j in 1..input){
                if (j%2==0){
                    print("1")
                }else{
                    print("0")
                }
            }
        }
        println()
    }
}